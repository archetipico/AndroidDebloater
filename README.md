# AndroidDebloater
Android debloater specifically created for Samsung Galaxy Note10 running Android 11.

## Requirements
Just ADB, no rooted device.

## Applications
<ul>
    <li>Save battery life and use less electricity for a greener future</li>
    <li>Make your device run faster</li>
    <li>Remove creepy trackers and unwanted personalization features: no one should own and use your data to interfere with your choices (Article 12 of Human Rights), and no one should use your private information such as your facial features, disabilities, political orientation,... to create targeted advertisements</li>
    <li>Study how the Android system works and what each service does</li>
</ul>

## Usage
<ol>
	<li>Clone this project</li>
	<li>Open a terminal</li>
	<li>Move to the cloned repository</li>
	<li>Be sure your device is connected through ADB</li>
	<li>Run <code>cat bloaters | xargs -I{} adb shell pm uninstall -k --user 0 {}</code></li>
</ol>

This will uninstall services for <code>user 0</code> (you).
If something breaks, reinstall the missing package with <code>adb shell cmd package install-existing \<package\></code>.

If you want to undo everything, just run <code>cat bloaters | xargs -I{} adb shell cmd package install-existing {}</code>

## Final notes
Maybe you'll be luckier than me, but when I try to remove <code>com.samsung.android.game.gos</code> it keeps reappearing in my system. I don't need that application and it has way too many permissions, so if someone managed to at least disable it please send me a pull request and I'll be happy to accept it.
